const githubData = {
  pullRequests: [
    {
      merged_at: "2018-07-26T19:12:05Z",
      title: "PR 2"
    },
    {
      merged_at: "2018-07-24T19:12:05Z",
      title: "PR 1"
    },
    {
      title: "PR 3 (Not merged)"
    }
  ]
};

export default githubData;
